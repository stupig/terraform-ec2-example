#!/bin/bash

set -e

# Update Ubuntu package
sudo apt update -y

# Installs MySQL
sudo apt install -y mysql-server mysql-client

# Configures MySQL
echo "MySQL Password set to '${MYSQL_PASSWORD}'."
sudo mysqladmin -u root password $MYSQL_PASSWORD
sudo service mysql restart

# Insert records
sudo mysql -uroot -p$MYSQL_PASSWORD << EOF
CREATE DATABASE hello;

USE hello;

CREATE TABLE hello_world (id bigint NOT NULL AUTO_INCREMENT, value varchar(128) NULL, primary key (id) );

INSERT INTO hello_world (value)
VALUES ('Hello 1');
INSERT INTO hello_world (value)
VALUES ('Hello 2');
INSERT INTO hello_world (value)
VALUES ('Hello 3');
EOF

# Cleaning unneeded packages
sudo apt-get autoremove -y
sudo apt-get clean

