resource "aws_security_group" "ec2_instance" {
  name = "instance-sg"
  description = "allow access to ec2 instance"
  vpc_id = aws_vpc.main.id
  ingress {
    protocol = "tcp"
    from_port = 22
    to_port = 22
    # This sg should only whitelist IP from verify organization
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    protocol = "tcp"
    from_port = 443
    to_port = 443
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}