# terraform-ec2-example

Terraform script to launch an ec2 instance in AWS
List variables have to override before run `terraform apply`:
- aws_id=<your_aws_id>
- aws_profile=<your_aws_certificate_profile>
- ec2_public_key=<your_generated_public_key>
- mysql_password=<your_my_sql_password>