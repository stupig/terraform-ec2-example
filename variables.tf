variable "aws_id" {
  description = "AWS account canonical ID"
  default = "your-aws-account-id"
}

variable "aws_region" {
  description = "The AWS region things are created in"
  default = "us-east-1"
}

variable "aws_profile" {
  description = "AWS certification profile"
  default = "default"
}

variable "availability_zone" {
  description = "The AWS AZ to start resources in"
  default = "us-east-1a"
}

variable "ec2_instance_type" {
  description = "EC2 instance type"
  default = "t2.micro"
}

variable "ec2_key_name" {
  description = "AWS EC2 key pair name which is used for ssh access"
  default = "ec2-ssh"
}

variable "ec2_public_key" {
  description = "AWS EC2 public key which is used for ssh access"
  default = "your ec2 public key"
}

variable "az_count" {
  description = "Number of AZs to cover in given region"
  default = "1"
}

variable "mysql_password" {
  description = "MySQL password"
  default = "you-never-know"
}