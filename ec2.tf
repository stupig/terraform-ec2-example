resource "aws_key_pair" "ec2_ssh_key" {
  key_name = var.ec2_key_name
  public_key = var.ec2_public_key
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = [var.aws_id] # Canonical
}

resource "aws_instance" "instance" {
  count = var.az_count
  ami = data.aws_ami.ubuntu.id
  instance_type = var.ec2_instance_type
  associate_public_ip_address = true
  ebs_optimized = false
  subnet_id = element(aws_subnet.public[*].id, count.index)
  vpc_security_group_ids = [
    aws_security_group.ec2_instance.id]
  user_data = templatefile("./templates/ec2_init_setup.sh", {
    MYSQL_PASSWORD = var.mysql_password
  })
  key_name = aws_key_pair.ec2_ssh_key.key_name
  availability_zone = var.availability_zone
}

resource "aws_ebs_volume" "storage" {
  availability_zone = var.availability_zone
  size = 20
}

resource "aws_volume_attachment" "ebs_att" {
  count = var.az_count
  device_name = "/dev/sdh"
  volume_id = aws_ebs_volume.storage.id
  instance_id = element(aws_instance.instance[*].id, count.index)
}


